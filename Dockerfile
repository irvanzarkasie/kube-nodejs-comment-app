FROM node:8-alpine

RUN npm install express

RUN npm install request

RUN npm install dotenv

RUN npm install uuid

COPY env.docker.v2 .env

COPY kube-comment-app.js .

CMD node kube-comment-app.js >> ./logs/kube-comment-app.out

EXPOSE 3000
